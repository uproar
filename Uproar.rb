#!/usr/bin/env ruby
# uproar.rb - search a YAML-encoded list of links for matching tags
# version 0.2
# see http://ardekantur.com/misc/uproar for information.

%w[ rubygems yaml open-uri hpricot ].each { |x| require x }

def set_option hash, name, default
  i = ARGV.index(name)
  hash[name] = i ? ARGV[i + 1] : default
end
 
module Uproar
 
  def Uproar.search_tags file, tag
    links = File.open( file )
    db = []
    YAML::load_documents( links ) { |doc| db << doc }
    db.each { |l| printf "%s\n %s\n", l['title'], l['link'] if l['tags'].include? tag }
  end
  
  def Uproar.add_link file, address, tags
    begin
      doc = Hpricot(open(address))
      title = doc.at("title").inner_html.strip
      puts "Adding '#{title}' to #{file}..."
    rescue
      $stderr.puts "site could not be reached."
      exit
    end
  
    link = { "title" => title, "link" => address, "tags" => ( tags ? tags.strip.gsub(/ /, ', ') : '' ) }
  
    links = File.new( file, "a" )
    links.puts link.to_yaml
    links.close
  end
end

if __FILE__ == $0

  o = {}
  set_option o, '-s', nil
  set_option o, '-f', 'Links.yaml'
  set_option o, '-t', ''
  set_option o, '-a', nil
  
  if (!o['-s'] and !o['-a']) or (o['-s'] and o['-a'])
    $stderr.puts "please specify either a tag to search for or a link to add."
    exit
  end
  
  Uproar.search_tags o['-f'], o['-s'] if o['-s']
  Uproar.add_link o['-f'], o['-a'], o['-t'] if o['-a']

end

